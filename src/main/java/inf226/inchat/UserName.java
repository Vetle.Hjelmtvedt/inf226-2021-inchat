package inf226.inchat;

public class UserName {

    private String userName;

    public UserName(String userName){
        this.userName = userName;
    }

    @Override
    public String toString() {
        return userName;
    }
}
