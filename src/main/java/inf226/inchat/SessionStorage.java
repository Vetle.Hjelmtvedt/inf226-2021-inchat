package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.UUID;

import inf226.storage.*;

/**
 * The SessionStorage stores Session objects in a SQL database.
 */
public final class SessionStorage
    implements Storage<Session,SQLException> {
    
    final Connection connection;
    final Storage<Account,SQLException> accountStorage;
    
    public SessionStorage(Connection connection,
                          Storage<Account,SQLException> accountStorage)
      throws SQLException {
        this.connection = connection;
        this.accountStorage = accountStorage;
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Session (id TEXT PRIMARY KEY, version TEXT, account TEXT, expiry TEXT, FOREIGN KEY(account) REFERENCES Account(id) ON DELETE CASCADE)");
    }
    
    @Override
    public Stored<Session> save(Session session)
      throws SQLException {
        
        final Stored<Session> stored = new Stored<Session>(session);

        PreparedStatement statement = connection.prepareStatement("INSERT INTO Session VALUES(?,?,?,?)");
        statement.setObject(1, stored.identity);
        statement.setObject(2, stored.version);
        statement.setObject(3, session.account.identity);
        statement.setString(4, session.expiry.toString());
        statement.executeUpdate();
        return stored;
    }
    
    @Override
    public synchronized Stored<Session> update(Stored<Session> session,
                                            Session new_session)
        throws UpdatedException,
            DeletedException,
            SQLException {
    final Stored<Session> current = get(session.identity);
    final Stored<Session> updated = current.newVersion(new_session);
    if(current.version.equals(session.version)) {

        PreparedStatement statement = connection.prepareStatement("UPDATE Session SET (version,account,expiry) =(?,?,?) WHERE id=?");
        statement.setObject(1, updated.version);
        statement.setObject(2, new_session.account.identity);
        statement.setString(3, new_session.expiry.toString());
        statement.setObject(4, updated.identity);
        statement.executeUpdate();
    } else {
        throw new UpdatedException(current);
    }
    return updated;
    }
   
    @Override
    public synchronized void delete(Stored<Session> session)
       throws UpdatedException,
              DeletedException,
              SQLException {
        final Stored<Session> current = get(session.identity);
        if(current.version.equals(session.version)) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM Session WHERE id =?");
            statement.setObject(1, session.identity);
            statement.executeUpdate();
        } else {
        throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<Session> get(UUID id)
      throws DeletedException,
             SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT version,account,expiry FROM Session WHERE id = ?");
        statement.setString(1, id.toString());
        final ResultSet rs = statement.executeQuery();


        if(rs.next()) {
            final UUID version = UUID.fromString(rs.getString("version"));
            final Stored<Account> account
               = accountStorage.get(
                    UUID.fromString(rs.getString("account")));
            final Instant expiry = Instant.parse(rs.getString("expiry"));
            return (new Stored<Session>
                        (new Session(account,expiry),id,version));
        } else {
            throw new DeletedException();
        }
    }
    
    
} 
