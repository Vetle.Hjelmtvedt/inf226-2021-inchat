package inf226.inchat;
import java.time.Instant;

/**
 * The User class holds the public information
 * about a user.
 **/
public final class User {
    public final UserName userName;
    public final Instant joined;

    public User(UserName userName,
                Instant joined) {
        this.userName = userName;
        this.joined = joined;
    }
    
    /**
     * Create a new user.
     */
    public static User create(String name) {
        return new User(new UserName(name), Instant.now());
    }
}

