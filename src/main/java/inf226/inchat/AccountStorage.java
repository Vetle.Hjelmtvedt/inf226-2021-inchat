package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.UUID;

import inf226.storage.*;

import inf226.util.immutable.List;
import inf226.util.*;

/**
 * This class stores accounts in the database.
 */
public final class AccountStorage
    implements Storage<Account,SQLException> {
    
    final Connection connection;
    final Storage<User,SQLException> userStore;
    final Storage<Channel,SQLException> channelStore;
   
    /**
     * Create a new account storage.
     *
     * @param  connection   The connection to the SQL database.
     * @param  userStore    The storage for User data.
     * @param  channelStore The storage for channels.
     */
    public AccountStorage(Connection connection,
                          Storage<User,SQLException> userStore,
                          Storage<Channel,SQLException> channelStore) 
      throws SQLException {
        this.connection = connection;
        this.userStore = userStore;
        this.channelStore = channelStore;
        
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Account (id TEXT PRIMARY KEY, version TEXT, user TEXT, password TEXT, FOREIGN KEY(user) REFERENCES User(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS AccountChannel (account TEXT, channel TEXT, alias TEXT, ordinal INTEGER, role TEXT, PRIMARY KEY(account,channel), FOREIGN KEY(account) REFERENCES Account(id) ON DELETE CASCADE, FOREIGN KEY(channel) REFERENCES Channel(id) ON DELETE CASCADE)");
    }
    
    @Override
    public Stored<Account> save(Account account)
      throws SQLException {
        
        final Stored<Account> stored = new Stored<Account>(account);
        PreparedStatement statement = connection.prepareStatement("INSERT INTO Account VALUES(?,?,?,?)");
        statement.setObject(1, stored.identity);
        statement.setObject(2, stored.version);
        statement.setObject(3, account.user.identity);
        statement.setString(4, account.hashedPassword);

        statement.executeUpdate();
        
        // Write the list of channels
        final Maybe.Builder<SQLException> exception = Maybe.builder();
        final Mutable<Integer> ordinal = new Mutable<Integer>(0);
        account.channels.forEach(element -> {
            String alias = element.first;
            Stored<Channel> channel = element.second;
            Role role = element.third;
            try {
                PreparedStatement statement1 = connection.prepareStatement("INSERT INTO AccountChannel VALUES(?,?,?,?,?");
                statement1.setObject(1, stored.identity);
                statement1.setObject(2, channel.identity);
                statement1.setString(3, alias);
                statement1.setString(4, ordinal.get().toString());
                statement1.setString(5, role.toString());

                //Execute statement
                statement1.executeUpdate();
            } catch (SQLException e) { exception.accept(e); }

            ordinal.accept(ordinal.get() + 1);
        });

        Util.throwMaybe(exception.getMaybe());
        return stored;
    }
    
    @Override
    public synchronized Stored<Account> update(Stored<Account> account,
                                            Account new_account)
        throws UpdatedException,
            DeletedException,
            SQLException {
    final Stored<Account> current = get(account.identity);
    final Stored<Account> updated = current.newVersion(new_account);
    if(current.version.equals(account.version)) {
        PreparedStatement statement = connection.prepareStatement("UPDATE Account SET  (version,user) =(?,?) WHERE id=?");
        statement.setObject(1, updated.version);
        statement.setObject(2, new_account.user.identity);
        statement.setObject(3, updated.identity);

        statement.executeUpdate();
        
        
        // Rewrite the list of channels
        PreparedStatement deleteStmt = connection.prepareStatement("DELETE FROM AccountChannel WHERE account=?");
        deleteStmt.setObject(1, account.identity);
        deleteStmt.executeUpdate();

        final Maybe.Builder<SQLException> exception = Maybe.builder();
        final Mutable<Integer> ordinal = new Mutable<Integer>(0);
        new_account.channels.forEach(element -> {
            String alias = element.first;
            Stored<Channel> channel = element.second;
            Role role = element.third;
            try {
                PreparedStatement statement1 = connection.prepareStatement("INSERT INTO AccountChannel VALUES(?,?,?,?,?)");
                statement1.setObject(1, account.identity);
                statement1.setObject(2, channel.identity);
                statement1.setString(3, alias);
                statement1.setString(4, ordinal.get().toString());
                statement1.setString(5,role.toString());

                // Execute statement
                statement1.executeUpdate();
            } catch (SQLException e) { exception.accept(e); }
            ordinal.accept(ordinal.get() + 1);
        });

        Util.throwMaybe(exception.getMaybe());
    } else {
        throw new UpdatedException(current);
    }
    return updated;
    }
   
    @Override
    public synchronized void delete(Stored<Account> account)
       throws UpdatedException,
              DeletedException,
              SQLException {
        final Stored<Account> current = get(account.identity);
        if(current.version.equals(account.version)) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM Account WHERE id =?");
            statement.setObject(1, account.identity);

            statement.executeUpdate();
        } else {
        throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<Account> get(UUID id)
      throws DeletedException,
             SQLException {

        PreparedStatement accountStmt = connection.prepareStatement("SELECT version,user,password FROM Account WHERE id =?");
        accountStmt.setString(1, id.toString());

        PreparedStatement channelStmt = connection.prepareStatement("SELECT channel,alias,ordinal,role FROM AccountChannel WHERE account = ? ORDER BY ordinal DESC");
        channelStmt.setString(1, id.toString());


        final ResultSet accountResult = accountStmt.executeQuery();
        final ResultSet channelResult = channelStmt.executeQuery();

        if(accountResult.next()) {
            final UUID version = UUID.fromString(accountResult.getString("version"));
            final UUID userid =
            UUID.fromString(accountResult.getString("user"));
            final String password =
            accountResult.getString("password");
            final Stored<User> user = userStore.get(userid);
            // Get all the channels associated with this account
            final List.Builder<Triple<String,Stored<Channel>,Role>> channels = List.builder();
            while(channelResult.next()) {
                final UUID channelId = 
                    UUID.fromString(channelResult.getString("channel"));
                final String alias = channelResult.getString("alias");
                final Role role = Role.valueOf(channelResult.getString("role"));
                channels.accept(
                    new Triple<String,Stored<Channel>,Role>(
                        alias,channelStore.get(channelId), role));
            }
            return (new Stored<Account>(new Account(user,channels.getList(),password),id,version));
        } else {
            throw new DeletedException();
        }
    }
    
    /**
     * Look up an account based on their username.
     */
    public Stored<Account> lookup(String username)
      throws DeletedException,
             SQLException {
        PreparedStatement stmt = connection.prepareStatement("SELECT Account.id from Account INNER JOIN User ON user=User.id where User.name=?");
        stmt.setString(1, username);


        final ResultSet rs = stmt.executeQuery();
        if(rs.next()) {
            final UUID identity = 
                    UUID.fromString(rs.getString("id"));
            return get(identity);
        }
        throw new DeletedException();
    }
    
} 
 
