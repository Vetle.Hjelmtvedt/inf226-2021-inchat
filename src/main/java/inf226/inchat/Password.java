package inf226.inchat;

public class Password {

    private String password;

    public Password(String password) throws IllegalArgumentException{
        if (checkPassword(password)) {
            this.password = password;
        } else {
            throw new IllegalArgumentException("Invalid Password");
        }
    }

    private boolean checkPassword(String password) {
        return password.length() >= 8 && password.length() < 1000;
    }

    @Override
    public String toString() {
        return password;
    }
}
