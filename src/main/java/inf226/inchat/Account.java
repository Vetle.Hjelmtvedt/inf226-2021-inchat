package inf226.inchat;
import com.lambdaworks.crypto.SCryptUtil;
import inf226.util.Triple;
import inf226.util.immutable.List;
import inf226.util.Pair;


import inf226.storage.*;

/**
 * The Account class holds all information private to
 * a specific user.
 **/
public final class Account {
    /*
     * A channel consists of a User object of public account info,
     * and a list of channels which the user can post to.
     */
    public final Stored<User> user;
    public final List<Triple<String,Stored<Channel>,Role>> channels;
    public final String hashedPassword;

    
    public Account(final Stored<User> user,
                   final List<Triple<String,Stored<Channel>,Role>> channels,
                   final String hashedPassword) {
        this.user = user;
        this.channels = channels;
        // Hash password with Scrypt
        this.hashedPassword = hashedPassword;
    }
    
    /**
     * Create a new Account.
     *
     * @param user The public User profile for this user.
     * @param password The login password for this account.
     **/
    public static Account create(final Stored<User> user,
                                 final Password password) {
        return new Account(user,List.empty(), SCryptUtil.scrypt(password.toString(), 16384, 8, 1));
    }
    
    /**
     * Join a channel with this account.
     *
     * @return A new account object with the channel added.
     */
    public Account joinChannel(final String alias, final Stored<Channel> channel, final Role role) {
        Triple<String,Stored<Channel>,Role> entry = new Triple<String,Stored<Channel>,Role>(alias,channel, role);
        return new Account (user,List.cons(entry,channels),hashedPassword);
    }


    /**
     * Check weather if a string is a correct password for
     * this account.
     *
     * @return true if password matches.
     */
    public boolean checkPassword(String password) {
        // Use scrypt to check if hashes match
        return SCryptUtil.check(password, this.hashedPassword);
    }
    
    
}
