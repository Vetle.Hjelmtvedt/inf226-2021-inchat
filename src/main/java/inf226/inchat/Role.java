package inf226.inchat;

public enum Role {
    OWNER,
    MODERATOR,
    PARTICIPANT,
    OBSERVER,
    BANNED
}
