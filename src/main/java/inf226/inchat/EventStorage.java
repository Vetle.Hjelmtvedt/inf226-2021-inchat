package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.UUID;
import java.util.function.Consumer;

import inf226.storage.*;
import inf226.util.*;




public final class EventStorage
    implements Storage<Channel.Event,SQLException> {
    
    private final Connection connection;
    
    public EventStorage(Connection connection) 
      throws SQLException {
        this.connection = connection;
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Event (id TEXT PRIMARY KEY, version TEXT, channel TEXT, type INTEGER, time TEXT, FOREIGN KEY(channel) REFERENCES Channel(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Message (id TEXT PRIMARY KEY, sender TEXT, content Text, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Joined (id TEXT PRIMARY KEY, sender TEXT, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
    }
    
    @Override
    public Stored<Channel.Event> save(Channel.Event event)
      throws SQLException {
        
        final Stored<Channel.Event> stored = new Stored<Channel.Event>(event);

        PreparedStatement statement = connection.prepareStatement("INSERT INTO Event VALUES(?,?,?,?,?)");
        statement.setObject(1, stored.identity);
        statement.setObject(2, stored.version);
        statement.setObject(3, event.channel);
        statement.setInt(4, event.type.code);
        statement.setObject(5, event.time);

        statement.executeUpdate();


        PreparedStatement eventStmt = null;
        switch (event.type) {
            case message:
                eventStmt = connection.prepareStatement("INSERT INTO Message VALUES(?,?,?)");
                eventStmt.setObject(1, stored.identity);
                eventStmt.setString(2, event.sender);
                eventStmt.setString(3, event.message);
                break;
            case join:
                eventStmt = connection.prepareStatement("INSERT INTO Joined VALUES(?,?)");
                eventStmt.setObject(1, stored.identity);
                eventStmt.setString(2, event.sender);
                break;
        }
        eventStmt.executeUpdate();
        return stored;
    }
    
    @Override
    public synchronized Stored<Channel.Event> update(Stored<Channel.Event> event,
                                            Channel.Event new_event)
        throws UpdatedException,
            DeletedException,
            SQLException {
    final Stored<Channel.Event> current = get(event.identity);
    final Stored<Channel.Event> updated = current.newVersion(new_event);
    if(current.version.equals(event.version)) {

        PreparedStatement statement = connection.prepareStatement("UPDATE Event SET (version,channel,time,type) =(?,?,?,?) WHERE id=?");
        statement.setObject(1, updated.version);
        statement.setObject(2, new_event.channel);
        statement.setObject(3, new_event.time);
        statement.setInt(4, new_event.type.code);
        statement.setObject(5, updated.identity);

        statement.executeUpdate();


        PreparedStatement eventStmt = null;
        switch (new_event.type) {
            case message:
                eventStmt = connection.prepareStatement("UPDATE Message SET (sender,content)=(?,?) WHERE id=?");
                eventStmt.setString(1, new_event.sender);
                eventStmt.setString(2, new_event.message);
                eventStmt.setObject(3, updated.identity);
                break;
            case join:
                eventStmt = connection.prepareStatement("UPDATE Joined SET (sender)=(?) WHERE id=?");
                eventStmt.setString(1, new_event.sender);
                eventStmt.setObject(2, updated.identity);
                break;
        }
        eventStmt.executeUpdate();
    } else {
        throw new UpdatedException(current);
    }
        return updated;
    }
   
    @Override
    public synchronized void delete(Stored<Channel.Event> event)
       throws UpdatedException,
              DeletedException,
              SQLException {
        final Stored<Channel.Event> current = get(event.identity);
        if(current.version.equals(event.version)) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM Event WHERE id =?");
            statement.setObject(1, event.identity);
            statement.executeUpdate();
        } else {
        throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<Channel.Event> get(UUID id)
      throws DeletedException,
             SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT version,channel,time,type FROM Event WHERE id = ?");
        statement.setString(1, id.toString());

        final ResultSet rs = statement.executeQuery();

        if(rs.next()) {
            final UUID version = UUID.fromString(rs.getString("version"));
            final UUID channel = 
                UUID.fromString(rs.getString("channel"));
            final Channel.Event.Type type = 
                Channel.Event.Type.fromInteger(rs.getInt("type"));
            final Instant time = 
                Instant.parse(rs.getString("time"));
            
            PreparedStatement mstatement = null;
            switch(type) {
                case message:
                    mstatement = connection.prepareStatement("SELECT sender,content FROM Message WHERE id = ?");
                    mstatement.setString(1, id.toString());
                    final ResultSet mrs = mstatement.executeQuery();
                    mrs.next();
                    return new Stored<Channel.Event>(
                            Channel.Event.createMessageEvent(channel,time,mrs.getString("sender"),mrs.getString("content")),
                            id,
                            version);
                case join:
                    mstatement = connection.prepareStatement("SELECT sender FROM Joined WHERE id = ?");
                    mstatement.setString(1, id.toString());
                    final ResultSet ars = mstatement.executeQuery();
                    ars.next();
                    return new Stored<Channel.Event>(
                            Channel.Event.createJoinEvent(channel,time,ars.getString("sender")),
                            id,
                            version);
            }
        }
        throw new DeletedException();
    }
    
}


 
