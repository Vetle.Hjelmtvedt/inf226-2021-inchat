package inf226.util;

import java.util.Objects;

public class Triple<A,B,C> {
    public final A first;
    public final B second;
    public final C third;

    public Triple(A first, B second, C third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Triple<?, ?, ?> triple1 = (Triple<?, ?, ?>) o;
        return Objects.equals(first, triple1.first) && Objects.equals(second, triple1.second) && Objects.equals(third, triple1.third);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second, third);
    }
}
